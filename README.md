# T432_LIY20_T3_E

## Serious Game Maker

>Serious Game Maker est un jeu de gestion où l'on doit concevoir le meilleur jeu sérieux possible

## Prérequis

- Git (avec un rôle attribué)

## Installation

1. 
```
git clone https://git.unistra.fr/la-team/T432_LIY20_T3_E 
```
2. Lancer le fichier **SGMSetup.msi** qui se trouve dans le dossier : ```votrerepertoirededestination\T432_LIY20_T3_E\Installation\```
3. Suivre les étapes de l'installation, et choisir le répertoire d'installation

## Démarrage

- Ouvrir le fichier **ProjetTutore.exe** se trouvant dans le repertoire d'installation

- Si l'installation s'est bien déroulée vous devriez arriver sur cette fenêtre : ![Screen](Images/Screenshot.PNG)

## Accès à la documentation de l'application

 Ouvrir le fichier index.html se trouvant dans le repertoire : ```votrerepertoirededestination\T432_LIY20_T3_E\DocumentationWeb\html\```

## Accès au code source

 Ouvrir le fichier ProjetTutore.cs se trouvant dans le repertoire : ```votrerepertoirededestination\T432_LIY20_T3_E\Developpement\```

## Outils utlisés

- [Visual Studio 2019](https://visualstudio.microsoft.com/fr/)
- [Microsoft Access](https://www.microsoft.com/fr-fr/microsoft-365/p/access/cfq7ttc0k7q8?activetab=pivot%3aoverviewtab)
- [Doxygen](https://www.doxygen.nl/index.html)
- [Installer Project](https://marketplace.visualstudio.com/items?itemName=VisualStudioClient.MicrosoftVisualStudio2017InstallerProjects#overview)
- [Font Awesome](https://github.com/awesome-inc/FontAwesome.Sharp)
- [Visual Studio Live Share](https://visualstudio.microsoft.com/fr/services/live-share/)

## Auteurs

- @vriehl
- @flejou
- @yberthier
- @bayi