Sous-ensemble des objectifs pédagogiques traités dans notre jeu sérieux : 
1) Équilibre défi-habilité :
    - Trouver un juste milieu entre un jeu trop difficile et un jeu trop ennuyeux afin de présenter un challenge adapté au niveau du joueur. 
2) Feedback :
    - Le retour de l’expérience de jeu de l’utilisateur, pouvant être modélisé par une note ou appréciation par exemple.
3) Expliquer ce qu'est l'affordance :
    - Possibilité d'action pour un joueur dans son evironnement de jeu. 

Description sommaire du jeu : 
Nous allons créer un jeu de type Game Dev Tycoon. 
Le but du joueur est de créer le meilleur jeu sérieux possible dans un temps limité in-game : le joueur devra prendre des décisions sur une durée maximale de 10 jours (Les jours ne sont pas timés : le joueur peut prendre son temps pour prendre ses décisions).

Action du joueur : 
Le joueur change des paramètres qui influent sur la qualité de son jeu sérieux. 
Pour tester la qualité de son jeu, le joueur peut réaliser des tests (nombre limité).
Chaque décisions coûtera de l'argent au joueur. 

Informations que le jeu renvoit au joueur : 
A la fin des phases de test, le jeu renvoit un résultat provisoire pour aiguiller le joueur. 
A la fin du jeu, en fonction de la qualité de ses choix, un nombre de points sera attribué au joueur avec quelques explications sur ses différents choix. 
