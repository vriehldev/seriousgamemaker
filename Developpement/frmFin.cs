﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetTutore
{
    /// <summary>
    /// Formulaire de la note finale du jeu 
    /// </summary>
    public partial class frmFin : Form
    {
        /// <summary>
        /// Liste contenant les valeurs des différentes décisions 
        /// </summary>
        List<int> valeurs;

        /// <summary>
        /// Tables contenant les informations des décisions 
        /// </summary>
        DataSet ds_decisions;

        /// <summary>
        /// Indique la cible du jeu sérieux créé
        /// </summary>
        /// <remarks>La cible vaut 0 pour la cible "Adulte" ou 1 pour la cible "Enfant"</remarks>
        int codeCible;

        /// <summary>
        /// Constructeur du formulaire de la note finale du jeu 
        /// </summary>
        /// <param name="val">Liste des valeurs des décisions</param>
        /// <param name="ds">Table contenant les informations des décisions</param>
        /// <param name="c">Cible</param>
        public frmFin(List<int> val, DataSet ds, int c)
        {
            InitializeComponent();
            try
            {
                valeurs = val;
                ds_decisions = ds;
                codeCible = c;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Récapitulatif des valeurs des différentes décisions prisent par le joueur
        /// </summary>
        private void recapValeur()
        {
            try
            {
                //Initialisation de la bonne image dans les pictureBox
                //Direction artistique :
                pcbDirAr.SizeMode = PictureBoxSizeMode.StretchImage;
                pcbDirAr.Image = System.Drawing.Image.FromFile(System.Windows.Forms.Application.StartupPath + @"\Images\pgb" + valeurs[1 - 1] + ".png");

                //Degré de liberté
                pcbDegLib.SizeMode = PictureBoxSizeMode.StretchImage;
                pcbDegLib.Image = System.Drawing.Image.FromFile(System.Windows.Forms.Application.StartupPath + @"\Images\pgb" + valeurs[2 - 1] + ".png");

                //Durée de vie
                pcbDureeVie.SizeMode = PictureBoxSizeMode.StretchImage;
                pcbDureeVie.Image = System.Drawing.Image.FromFile(System.Windows.Forms.Application.StartupPath + @"\Images\pgb" + valeurs[3 - 1] + ".png");

                //Feedback
                pcbFeedback.SizeMode = PictureBoxSizeMode.StretchImage;
                pcbFeedback.Image = System.Drawing.Image.FromFile(System.Windows.Forms.Application.StartupPath + @"\Images\pgb" + valeurs[4 - 1] + ".png");

                //Courbe de progression
                pcbCourbeProg.SizeMode = PictureBoxSizeMode.StretchImage;
                pcbCourbeProg.Image = System.Drawing.Image.FromFile(System.Windows.Forms.Application.StartupPath + @"\Images\pgb" + valeurs[5 - 1] + ".png");

                //Difficulté 
                pcbDifficulte.SizeMode = PictureBoxSizeMode.StretchImage;
                pcbDifficulte.Image = System.Drawing.Image.FromFile(System.Windows.Forms.Application.StartupPath + @"\Images\pgb" + valeurs[6 - 1] + ".png");

                //Quantité d'informations
                pcbQteInfos.SizeMode = PictureBoxSizeMode.StretchImage;
                pcbQteInfos.Image = System.Drawing.Image.FromFile(System.Windows.Forms.Application.StartupPath + @"\Images\pgb" + valeurs[7 - 1] + ".png");

                //Expérience active de maîtrise
                pcbExpActivMaitrise.SizeMode = PictureBoxSizeMode.StretchImage;
                pcbExpActivMaitrise.Image = System.Drawing.Image.FromFile(System.Windows.Forms.Application.StartupPath + @"\Images\pgb" + valeurs[8 - 1] + ".png");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Fonction qui calcule l'écart total entre les valeurs données par le joueur et les valeurs attendues pour chaque décision
        /// </summary>
        /// <returns>ecart</returns>
        public int calculEcart()
        {
            int ecart = 0;
            try
            {
                //On parcourt toutes les décisions 
                for (int i = 0; i < 8; i++)
                {
                    //On récupère toutes les informations sur la décison
                    DataRow[] infos = ds_decisions.Tables["Decisions"].Select("codeDec = " + (i + 1));

                    //Intialisation de la valeur attendue pour une décision
                    int valdefaut;

                    //On récupère chaque valeur attendue en fonction de la cible
                    if (codeCible == 0)
                    {
                        valdefaut = (int)infos[0]["valAd"];
                    }
                    else
                    {
                        valdefaut = (int)infos[0]["valEnf"];
                    }

                    //On calcule la différence entre la valeur attendue et la valeur du joueur pour une décision
                    int difference = Math.Abs(valdefaut - valeurs[i]);

                    //On l'ajoute à la totalité
                    ecart += difference;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return ecart;
        }

        /// <summary>
        /// Fonction qui calcule la note du joueur 
        /// </summary>
        /// <param name="ecart">ecart global</param>
        /// <returns>note</returns>
        public double calculNote(int ecart)
        {
            //Initialisation de la note
            double note = 20.0;
            try
            {
                //Calcul de la note en fonction de la cible du jeu
                if (codeCible == 0)
                {
                    note -= ecart / 2.0;
                }
                else
                {
                    note -= ecart / 3.0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return note;
        }

        /// <summary>
        /// Actions réalisées lors du chargement du formulaire 
        /// </summary>
        /// <param name="sender">Object ayant demandé l'évènement</param>
        /// <param name="e">Arguments d'évènement</param>
        private void frmFin_Load(object sender, EventArgs e)
        {
            try
            {
                //Appel de la procédure récapitulant les valeurs de chaque décision
                recapValeur();

                //Calcul de l'écart total
                int ecart = calculEcart();

                //Calcul de la note
                double note = Math.Round(calculNote(ecart));

                //Si la note est négative, on l'arrondi à 0
                if (note < 0)
                    note = 0;

                //Mise en place du message contenant note
                string message = note + "/20";

                //Affichage des messages des médias spécialisés en fonction de la note du joueur
                if (note == 20)
                {
                    lblJvs.Text = "\"Un futur classique du genre\" - Jeux Vidéo Sérieux";
                    lblSGN.Text = "\"Un véritable chef d'oeuvre\" - Serious Games Network";
                    lblLF.Text = "\"Le caviar du jeu sérieux\" - Learn and Fun";
                }
                else if (note < 20 && note >= 16)
                {
                    lblJvs.Text = "\"Une exécution soignée\" - Jeux Vidéo Sérieux";
                    lblSGN.Text = "\"Un vrai raffraichissement du genre !\" - Serious Games Network";
                    lblLF.Text = "\"L'un des meilleurs jeux de l'année\"\nLearn and Fun";
                }
                else if (note < 16 && note >= 10)
                {
                    lblJvs.Text = "\"Bon jeu malgré quelques faiblesses\" - Jeux Vidéo Sérieux";
                    lblSGN.Text = "\"Répond aux attentes des joueurs\" - Serious Games Network";
                    lblLF.Text = "\"A defaut d'innover, le jeu tient la route\"\nLearn and Fun";
                }
                else
                {
                    lblJvs.Text = "\"Mais qu'est ce que c'est que ça !\" - Jeux Vidéo Sérieux";
                    lblSGN.Text = "\"Ce jeu fait honte à la discipline\" - Serious Games Network";
                    lblLF.Text = "\"...\" - Learn and Fun";
                }
                lblFin.Text += " : " + message;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Lorsque le joueur clique sur le bouton "Fin"
        /// </summary>
        /// <param name="sender">Object ayant demandé l'évènement</param>
        /// <param name="e">Arguments d'évènement</param>
        private void btnAccueil_Click(object sender, EventArgs e)
        {
            try
            {
                //Le formulaire d'accueil est affiché
                FrmAccueil newForm = new FrmAccueil();
                newForm.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}