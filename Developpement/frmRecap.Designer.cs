﻿
namespace ProjetTutore
{
    partial class frmRecap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRecap));
            this.btnSuivant = new System.Windows.Forms.Button();
            this.pnlRecap = new System.Windows.Forms.Panel();
            this.pcbConseil = new System.Windows.Forms.PictureBox();
            this.pcbAttention = new System.Windows.Forms.PictureBox();
            this.lblDisp = new System.Windows.Forms.Label();
            this.lblConseil = new System.Windows.Forms.Label();
            this.pcbExpActivMaitrise = new System.Windows.Forms.PictureBox();
            this.pcbQteInfos = new System.Windows.Forms.PictureBox();
            this.pcbDifficulte = new System.Windows.Forms.PictureBox();
            this.pcbCourbeProg = new System.Windows.Forms.PictureBox();
            this.pcbFeedback = new System.Windows.Forms.PictureBox();
            this.pcbDureeVie = new System.Windows.Forms.PictureBox();
            this.pcbDegLib = new System.Windows.Forms.PictureBox();
            this.pcbDirAr = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblQteInfos = new System.Windows.Forms.Label();
            this.lblDifficulte = new System.Windows.Forms.Label();
            this.lblCourbeProg = new System.Windows.Forms.Label();
            this.lblFeedback = new System.Windows.Forms.Label();
            this.lblDureeVie = new System.Windows.Forms.Label();
            this.lblDegLib = new System.Windows.Forms.Label();
            this.lblDirArt = new System.Windows.Forms.Label();
            this.lblRecap = new System.Windows.Forms.Label();
            this.pnlRecap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbConseil)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbAttention)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbExpActivMaitrise)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbQteInfos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbDifficulte)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbCourbeProg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbFeedback)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbDureeVie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbDegLib)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbDirAr)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSuivant
            // 
            this.btnSuivant.BackColor = System.Drawing.Color.White;
            this.btnSuivant.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSuivant.Location = new System.Drawing.Point(912, 636);
            this.btnSuivant.Name = "btnSuivant";
            this.btnSuivant.Size = new System.Drawing.Size(176, 52);
            this.btnSuivant.TabIndex = 1;
            this.btnSuivant.Text = "Suivant ";
            this.btnSuivant.UseVisualStyleBackColor = false;
            this.btnSuivant.Click += new System.EventHandler(this.btnSuivant_Click);
            // 
            // pnlRecap
            // 
            this.pnlRecap.BackColor = System.Drawing.Color.White;
            this.pnlRecap.Controls.Add(this.pcbConseil);
            this.pnlRecap.Controls.Add(this.pcbAttention);
            this.pnlRecap.Controls.Add(this.lblDisp);
            this.pnlRecap.Controls.Add(this.lblConseil);
            this.pnlRecap.Controls.Add(this.pcbExpActivMaitrise);
            this.pnlRecap.Controls.Add(this.pcbQteInfos);
            this.pnlRecap.Controls.Add(this.pcbDifficulte);
            this.pnlRecap.Controls.Add(this.pcbCourbeProg);
            this.pnlRecap.Controls.Add(this.pcbFeedback);
            this.pnlRecap.Controls.Add(this.pcbDureeVie);
            this.pnlRecap.Controls.Add(this.pcbDegLib);
            this.pnlRecap.Controls.Add(this.pcbDirAr);
            this.pnlRecap.Controls.Add(this.label1);
            this.pnlRecap.Controls.Add(this.lblQteInfos);
            this.pnlRecap.Controls.Add(this.lblDifficulte);
            this.pnlRecap.Controls.Add(this.lblCourbeProg);
            this.pnlRecap.Controls.Add(this.lblFeedback);
            this.pnlRecap.Controls.Add(this.lblDureeVie);
            this.pnlRecap.Controls.Add(this.lblDegLib);
            this.pnlRecap.Controls.Add(this.lblDirArt);
            this.pnlRecap.Location = new System.Drawing.Point(12, 69);
            this.pnlRecap.Name = "pnlRecap";
            this.pnlRecap.Size = new System.Drawing.Size(1076, 554);
            this.pnlRecap.TabIndex = 2;
            // 
            // pcbConseil
            // 
            this.pcbConseil.Location = new System.Drawing.Point(42, 331);
            this.pcbConseil.Name = "pcbConseil";
            this.pcbConseil.Size = new System.Drawing.Size(68, 63);
            this.pcbConseil.TabIndex = 19;
            this.pcbConseil.TabStop = false;
            // 
            // pcbAttention
            // 
            this.pcbAttention.Location = new System.Drawing.Point(10, 420);
            this.pcbAttention.Name = "pcbAttention";
            this.pcbAttention.Size = new System.Drawing.Size(135, 117);
            this.pcbAttention.TabIndex = 18;
            this.pcbAttention.TabStop = false;
            // 
            // lblDisp
            // 
            this.lblDisp.AutoEllipsis = true;
            this.lblDisp.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDisp.Location = new System.Drawing.Point(151, 435);
            this.lblDisp.Name = "lblDisp";
            this.lblDisp.Size = new System.Drawing.Size(884, 87);
            this.lblDisp.TabIndex = 17;
            this.lblDisp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblConseil
            // 
            this.lblConseil.AutoEllipsis = true;
            this.lblConseil.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConseil.Location = new System.Drawing.Point(156, 331);
            this.lblConseil.Name = "lblConseil";
            this.lblConseil.Size = new System.Drawing.Size(897, 86);
            this.lblConseil.TabIndex = 16;
            // 
            // pcbExpActivMaitrise
            // 
            this.pcbExpActivMaitrise.Location = new System.Drawing.Point(821, 246);
            this.pcbExpActivMaitrise.Name = "pcbExpActivMaitrise";
            this.pcbExpActivMaitrise.Size = new System.Drawing.Size(250, 37);
            this.pcbExpActivMaitrise.TabIndex = 15;
            this.pcbExpActivMaitrise.TabStop = false;
            // 
            // pcbQteInfos
            // 
            this.pcbQteInfos.Location = new System.Drawing.Point(821, 179);
            this.pcbQteInfos.Name = "pcbQteInfos";
            this.pcbQteInfos.Size = new System.Drawing.Size(250, 37);
            this.pcbQteInfos.TabIndex = 14;
            this.pcbQteInfos.TabStop = false;
            // 
            // pcbDifficulte
            // 
            this.pcbDifficulte.Location = new System.Drawing.Point(821, 110);
            this.pcbDifficulte.Name = "pcbDifficulte";
            this.pcbDifficulte.Size = new System.Drawing.Size(250, 37);
            this.pcbDifficulte.TabIndex = 13;
            this.pcbDifficulte.TabStop = false;
            // 
            // pcbCourbeProg
            // 
            this.pcbCourbeProg.Location = new System.Drawing.Point(821, 40);
            this.pcbCourbeProg.Name = "pcbCourbeProg";
            this.pcbCourbeProg.Size = new System.Drawing.Size(250, 37);
            this.pcbCourbeProg.TabIndex = 12;
            this.pcbCourbeProg.TabStop = false;
            // 
            // pcbFeedback
            // 
            this.pcbFeedback.Location = new System.Drawing.Point(225, 246);
            this.pcbFeedback.Name = "pcbFeedback";
            this.pcbFeedback.Size = new System.Drawing.Size(250, 37);
            this.pcbFeedback.TabIndex = 11;
            this.pcbFeedback.TabStop = false;
            // 
            // pcbDureeVie
            // 
            this.pcbDureeVie.Location = new System.Drawing.Point(225, 179);
            this.pcbDureeVie.Name = "pcbDureeVie";
            this.pcbDureeVie.Size = new System.Drawing.Size(250, 37);
            this.pcbDureeVie.TabIndex = 10;
            this.pcbDureeVie.TabStop = false;
            // 
            // pcbDegLib
            // 
            this.pcbDegLib.Location = new System.Drawing.Point(225, 110);
            this.pcbDegLib.Name = "pcbDegLib";
            this.pcbDegLib.Size = new System.Drawing.Size(250, 37);
            this.pcbDegLib.TabIndex = 9;
            this.pcbDegLib.TabStop = false;
            // 
            // pcbDirAr
            // 
            this.pcbDirAr.Location = new System.Drawing.Point(225, 40);
            this.pcbDirAr.Name = "pcbDirAr";
            this.pcbDirAr.Size = new System.Drawing.Size(250, 37);
            this.pcbDirAr.TabIndex = 8;
            this.pcbDirAr.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(516, 254);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(299, 29);
            this.label1.TabIndex = 7;
            this.label1.Text = "Expérience active de maîtrise";
            // 
            // lblQteInfos
            // 
            this.lblQteInfos.AutoSize = true;
            this.lblQteInfos.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQteInfos.Location = new System.Drawing.Point(516, 187);
            this.lblQteInfos.Name = "lblQteInfos";
            this.lblQteInfos.Size = new System.Drawing.Size(248, 29);
            this.lblQteInfos.TabIndex = 6;
            this.lblQteInfos.Text = "Quantité d\'informations";
            // 
            // lblDifficulte
            // 
            this.lblDifficulte.AutoSize = true;
            this.lblDifficulte.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDifficulte.Location = new System.Drawing.Point(516, 118);
            this.lblDifficulte.Name = "lblDifficulte";
            this.lblDifficulte.Size = new System.Drawing.Size(102, 29);
            this.lblDifficulte.TabIndex = 5;
            this.lblDifficulte.Text = "Difficulté";
            // 
            // lblCourbeProg
            // 
            this.lblCourbeProg.AutoSize = true;
            this.lblCourbeProg.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCourbeProg.Location = new System.Drawing.Point(516, 48);
            this.lblCourbeProg.Name = "lblCourbeProg";
            this.lblCourbeProg.Size = new System.Drawing.Size(235, 29);
            this.lblCourbeProg.TabIndex = 4;
            this.lblCourbeProg.Text = "Courbe de progression";
            // 
            // lblFeedback
            // 
            this.lblFeedback.AutoSize = true;
            this.lblFeedback.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFeedback.Location = new System.Drawing.Point(3, 254);
            this.lblFeedback.Name = "lblFeedback";
            this.lblFeedback.Size = new System.Drawing.Size(107, 29);
            this.lblFeedback.TabIndex = 3;
            this.lblFeedback.Text = "Feedback";
            // 
            // lblDureeVie
            // 
            this.lblDureeVie.AutoSize = true;
            this.lblDureeVie.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDureeVie.Location = new System.Drawing.Point(3, 187);
            this.lblDureeVie.Name = "lblDureeVie";
            this.lblDureeVie.Size = new System.Drawing.Size(142, 29);
            this.lblDureeVie.TabIndex = 2;
            this.lblDureeVie.Text = "Durée de vie ";
            // 
            // lblDegLib
            // 
            this.lblDegLib.AutoSize = true;
            this.lblDegLib.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDegLib.Location = new System.Drawing.Point(3, 118);
            this.lblDegLib.Name = "lblDegLib";
            this.lblDegLib.Size = new System.Drawing.Size(176, 29);
            this.lblDegLib.TabIndex = 1;
            this.lblDegLib.Text = "Degré de liberté ";
            // 
            // lblDirArt
            // 
            this.lblDirArt.AutoSize = true;
            this.lblDirArt.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDirArt.Location = new System.Drawing.Point(3, 48);
            this.lblDirArt.Name = "lblDirArt";
            this.lblDirArt.Size = new System.Drawing.Size(201, 29);
            this.lblDirArt.TabIndex = 0;
            this.lblDirArt.Text = "Direction artistique";
            // 
            // lblRecap
            // 
            this.lblRecap.AutoSize = true;
            this.lblRecap.BackColor = System.Drawing.Color.Transparent;
            this.lblRecap.Font = new System.Drawing.Font("Calibri", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRecap.Location = new System.Drawing.Point(435, 9);
            this.lblRecap.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRecap.Name = "lblRecap";
            this.lblRecap.Size = new System.Drawing.Size(215, 45);
            this.lblRecap.TabIndex = 4;
            this.lblRecap.Text = "Récapitulatif";
            // 
            // frmRecap
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1100, 700);
            this.Controls.Add(this.lblRecap);
            this.Controls.Add(this.pnlRecap);
            this.Controls.Add(this.btnSuivant);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmRecap";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmRecap";
            this.Load += new System.EventHandler(this.frmRecap_Load);
            this.pnlRecap.ResumeLayout(false);
            this.pnlRecap.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbConseil)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbAttention)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbExpActivMaitrise)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbQteInfos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbDifficulte)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbCourbeProg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbFeedback)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbDureeVie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbDegLib)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbDirAr)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnSuivant;
        private System.Windows.Forms.Panel pnlRecap;
        private System.Windows.Forms.Label lblDirArt;
        private System.Windows.Forms.Label lblDegLib;
        private System.Windows.Forms.Label lblDureeVie;
        private System.Windows.Forms.Label lblFeedback;
        private System.Windows.Forms.Label lblCourbeProg;
        private System.Windows.Forms.Label lblDifficulte;
        private System.Windows.Forms.Label lblQteInfos;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pcbDirAr;
        private System.Windows.Forms.PictureBox pcbExpActivMaitrise;
        private System.Windows.Forms.PictureBox pcbQteInfos;
        private System.Windows.Forms.PictureBox pcbDifficulte;
        private System.Windows.Forms.PictureBox pcbCourbeProg;
        private System.Windows.Forms.PictureBox pcbFeedback;
        private System.Windows.Forms.PictureBox pcbDureeVie;
        private System.Windows.Forms.PictureBox pcbDegLib;
        private System.Windows.Forms.Label lblConseil;
        private System.Windows.Forms.Label lblDisp;
        private System.Windows.Forms.PictureBox pcbAttention;
        private System.Windows.Forms.Label lblRecap;
        private System.Windows.Forms.PictureBox pcbConseil;
    }
}