﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetTutore
{
    /// <summary>
    /// Formulaire récapitulatif 
    /// </summary>
    public partial class frmRecap : Form
    {

        /// <summary>
        /// Liste contenant les valeurs des différentes décisions 
        /// </summary>
        List<int> valeurs;

        /// <summary>
        /// Ligne contenant toute les informations d'une décision 
        /// </summary>
        DataRow[] infoDecision;
        /// <summary>
        /// Tables contenant les informations des décisions 
        /// </summary>
        DataSet ds;

        /// <summary>
        /// Indique la cible du jeu sérieux créé
        /// </summary>
        /// <remarks>La cible vaut 0 pour la cible "Adulte" ou 1 pour la cible "Enfant"</remarks>
        int codeCible;

        /// <summary>
        /// Indique le numéro du jour suivant 
        /// </summary>
        int jour;


        /// <summary>
        /// Constructeur du formulaire récapitulatif 
        /// </summary>
        /// <param name="val">Valeurs des décisions</param>
        /// <param name="info">Ligne contenant les informations d'une décision</param>
        /// <param name="ds_decisions">Tables contenant les informations de toutes les décisions</param>
        /// <param name="cible">Cible du jeu</param>
        /// <param name="nbjour">Numéro du jour suivant</param>
        public frmRecap(List<int> val, DataRow[] info, DataSet ds_decisions, int cible, int nbjour)
        {
            InitializeComponent();
            try
            {
                valeurs = val;
                infoDecision = info;
                ds = ds_decisions;
                codeCible = cible;
                jour = nbjour;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Lorsque le formulaire charge
        /// </summary>
        /// <param name="sender">Object ayant demandé l'évènement</param>
        /// <param name="e">Arguments d'évènement</param>
        private void frmRecap_Load(object sender, EventArgs e)
        {
            try
            {
                //Appel de la précédure qui récapitule les valeurs des décisions
                recapValeur();

                //Appel de la procédure donnant un conseil au joueur
                conseilJoueur();

                //Appel de la procédure avertissant le joueur de la disparition d'une décision
                disparitionDecision();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Récapitulatif des valeurs des différentes décisions prises par le joueur
        /// </summary>
        private void recapValeur()
        {
            try
            {
                //Initialisation de la bonne image dans les pictureBox
                //Direction artistique :
                pcbDirAr.SizeMode = PictureBoxSizeMode.StretchImage;
                pcbDirAr.Image = System.Drawing.Image.FromFile(System.Windows.Forms.Application.StartupPath + @"\Images\pgb" + valeurs[1 - 1] + ".png");

                //Degré de liberté
                pcbDegLib.SizeMode = PictureBoxSizeMode.StretchImage;
                pcbDegLib.Image = System.Drawing.Image.FromFile(System.Windows.Forms.Application.StartupPath + @"\Images\pgb" + valeurs[2 - 1] + ".png");

                //Durée de vie
                pcbDureeVie.SizeMode = PictureBoxSizeMode.StretchImage;
                pcbDureeVie.Image = System.Drawing.Image.FromFile(System.Windows.Forms.Application.StartupPath + @"\Images\pgb" + valeurs[3 - 1] + ".png");

                //Feedback
                pcbFeedback.SizeMode = PictureBoxSizeMode.StretchImage;
                pcbFeedback.Image = System.Drawing.Image.FromFile(System.Windows.Forms.Application.StartupPath + @"\Images\pgb" + valeurs[4 - 1] + ".png");

                //Courbe de progression
                pcbCourbeProg.SizeMode = PictureBoxSizeMode.StretchImage;
                pcbCourbeProg.Image = System.Drawing.Image.FromFile(System.Windows.Forms.Application.StartupPath + @"\Images\pgb" + valeurs[5 - 1] + ".png");

                //Difficulté
                pcbDifficulte.SizeMode = PictureBoxSizeMode.StretchImage;
                pcbDifficulte.Image = System.Drawing.Image.FromFile(System.Windows.Forms.Application.StartupPath + @"\Images\pgb" + valeurs[6 - 1] + ".png");

                //Quantité d'informations
                pcbQteInfos.SizeMode = PictureBoxSizeMode.StretchImage;
                pcbQteInfos.Image = System.Drawing.Image.FromFile(System.Windows.Forms.Application.StartupPath + @"\Images\pgb" + valeurs[7 - 1] + ".png");

                //Expérience active de maîtrise
                pcbExpActivMaitrise.SizeMode = PictureBoxSizeMode.StretchImage;
                pcbExpActivMaitrise.Image = System.Drawing.Image.FromFile(System.Windows.Forms.Application.StartupPath + @"\Images\pgb" + valeurs[8 - 1] + ".png");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Donne un conseil au joueur 
        /// </summary>
        private void conseilJoueur()
        {
            try
            {
                int max = 0;
                int code = 0;
                int valeur = 0;

                //Pour chaque décision du jour passé
                for (int i = 0; i < 4; i++)
                {
                    //On récupère le code de la décision
                    int codeDec = (int)infoDecision[i]["codeDec"];
                    int valdefaut;

                    //Intialisation de la valeur attendue pour une décision
                    if (codeCible == 0)
                    {
                        valdefaut = (int)infoDecision[i]["valAd"];
                    }
                    else
                    {
                        valdefaut = (int)infoDecision[i]["valEnf"];
                    }

                    //On calcule la différence entre la valeur du joueur et la valeur attendue
                    int difference = Math.Abs(valdefaut - valeurs[codeDec - 1]);

                    //On récupère la différence maximale, le code de la décision ainsi que le valeur attendue
                    if (max <= difference)
                    {
                        max = difference;
                        code = codeDec;
                        valeur = valdefaut;
                    }
                }

                //On récupère les informations sur la décision ayant le plus grand écart entre la valeur attendue et la valeur du joueur
                DataRow[] dr = ds.Tables["Decisions"].Select("codeDec = " + code);

                //On affiche un message en fonction de l'écart
                string message = "";
                if (valeurs[code - 1] > valeur && code != 0)
                {
                    message = "Vous accordez peut-être trop d'importance à " + dr[0]["nomDec"] + ". " + dr[0]["conseilPlus"];
                }
                else if (valeurs[code - 1] < valeur)
                {
                    message = "Vous n'accordez peut-être pas assez d'importance à " + dr[0]["nomDec"] + ". " + dr[0]["conseilMoins"];
                }
                else if (max < 2)
                {
                    message = "Vous êtes sur la bonne voie, continuez comme ça !";
                }
                lblConseil.Text = message;

                //On affiche une image (ampoule)
                pcbConseil.SizeMode = PictureBoxSizeMode.StretchImage;
                pcbConseil.Image = System.Drawing.Image.FromFile(System.Windows.Forms.Application.StartupPath + @"\Images\ampoule.png");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Avertissement au joueur lors de la disparition d'une décision 
        /// </summary>
        private void disparitionDecision()
        {
            try
            {
                //On cherche les informations qui concernent cette décision
                DataRow[] DecisionDisparition = ds.Tables["Decisions"].Select(jour + " = jourFin");

                //Si une décision va disparaître et que nous ne sommes pas encore au dernier jour de jeu 
                if (DecisionDisparition.Length != 0 && jour != 10)
                {
                    //On affiche un message 
                    lblDisp.Text = "Attention, " + DecisionDisparition[0]["nomDec"].ToString() + " va disparaître après le jour suivant.";
                    //On affiche une image (panneau attention)
                    pcbAttention.SizeMode = PictureBoxSizeMode.StretchImage;
                    pcbAttention.Image = System.Drawing.Image.FromFile(System.Windows.Forms.Application.StartupPath + @"\Images\warning.png");
                }
                //S'il s'agit du dernier jour du jeu
                else if (jour == 10)
                {
                    //On prévient le joueur que le jeu se termine après la prochaine journée
                    lblDisp.Text = "Attention, il ne vous reste plus qu'un jour avant la fin du jeu";
                    //On affiche une image (panneau attention)
                    pcbAttention.SizeMode = PictureBoxSizeMode.StretchImage;
                    pcbAttention.Image = System.Drawing.Image.FromFile(System.Windows.Forms.Application.StartupPath + @"\Images\warning.png");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Lorsque le joueur clique sur le bouton "Suivant"
        /// </summary>
        /// <param name="sender">Object ayant demandé l'évènement</param>
        /// <param name="e">Arguments d'évènement</param>
        private void btnSuivant_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}