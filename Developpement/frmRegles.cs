﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetTutore
{
    /// <summary>
    /// Formulaire des règles du jeu
    /// </summary>
    public partial class frmRegles : Form
    {
        /// <summary>
        /// Constructeur du formulaire des règles du jeu 
        /// </summary>
        public frmRegles()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Lorsque le joueur clique sur le bouton "Quitter"
        /// </summary>
        /// <param name="sender">Object ayant demandé l'évènement</param>
        /// <param name="e">Arguments d'évènement</param>
        private void btnQuitter_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
