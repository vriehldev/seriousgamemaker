﻿namespace ProjetTutore
{
    partial class FrmAccueil
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAccueil));
            this.btnQuitter = new System.Windows.Forms.Button();
            this.btnAdulte = new System.Windows.Forms.Button();
            this.btnEnfant = new System.Windows.Forms.Button();
            this.iPcbRegle = new FontAwesome.Sharp.IconPictureBox();
            this.lblBut = new System.Windows.Forms.Label();
            this.lblBtn = new System.Windows.Forms.Label();
            this.lblRegle = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.iPcbRegle)).BeginInit();
            this.SuspendLayout();
            // 
            // btnQuitter
            // 
            this.btnQuitter.BackColor = System.Drawing.Color.White;
            this.btnQuitter.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQuitter.Location = new System.Drawing.Point(12, 639);
            this.btnQuitter.Name = "btnQuitter";
            this.btnQuitter.Size = new System.Drawing.Size(158, 49);
            this.btnQuitter.TabIndex = 0;
            this.btnQuitter.Text = "Quitter";
            this.btnQuitter.UseVisualStyleBackColor = false;
            this.btnQuitter.Click += new System.EventHandler(this.btnQuitter_Click);
            // 
            // btnAdulte
            // 
            this.btnAdulte.BackColor = System.Drawing.Color.White;
            this.btnAdulte.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdulte.Location = new System.Drawing.Point(672, 482);
            this.btnAdulte.Name = "btnAdulte";
            this.btnAdulte.Size = new System.Drawing.Size(213, 57);
            this.btnAdulte.TabIndex = 1;
            this.btnAdulte.Text = "Adulte";
            this.btnAdulte.UseVisualStyleBackColor = false;
            this.btnAdulte.Click += new System.EventHandler(this.btnAdulte_Click);
            // 
            // btnEnfant
            // 
            this.btnEnfant.BackColor = System.Drawing.Color.White;
            this.btnEnfant.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnfant.Location = new System.Drawing.Point(279, 482);
            this.btnEnfant.Name = "btnEnfant";
            this.btnEnfant.Size = new System.Drawing.Size(213, 57);
            this.btnEnfant.TabIndex = 2;
            this.btnEnfant.Text = "Enfant ";
            this.btnEnfant.UseVisualStyleBackColor = false;
            this.btnEnfant.Click += new System.EventHandler(this.btnEnfant_Click);
            // 
            // iPcbRegle
            // 
            this.iPcbRegle.BackColor = System.Drawing.Color.Transparent;
            this.iPcbRegle.ForeColor = System.Drawing.SystemColors.ControlText;
            this.iPcbRegle.IconChar = FontAwesome.Sharp.IconChar.QuestionCircle;
            this.iPcbRegle.IconColor = System.Drawing.SystemColors.ControlText;
            this.iPcbRegle.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iPcbRegle.IconSize = 84;
            this.iPcbRegle.Location = new System.Drawing.Point(995, 604);
            this.iPcbRegle.Name = "iPcbRegle";
            this.iPcbRegle.Size = new System.Drawing.Size(93, 84);
            this.iPcbRegle.TabIndex = 3;
            this.iPcbRegle.TabStop = false;
            this.iPcbRegle.Click += new System.EventHandler(this.iPcbRegle_Click);
            // 
            // lblBut
            // 
            this.lblBut.BackColor = System.Drawing.Color.Transparent;
            this.lblBut.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBut.Location = new System.Drawing.Point(12, 390);
            this.lblBut.Name = "lblBut";
            this.lblBut.Size = new System.Drawing.Size(1076, 36);
            this.lblBut.TabIndex = 4;
            this.lblBut.Text = "Votre but est de créer un jeu sérieux. Pour cela la première étape consiste à cho" +
    "isir la cible de votre jeu :                            \r\n\r\n\r\n";
            // 
            // lblBtn
            // 
            this.lblBtn.AutoSize = true;
            this.lblBtn.BackColor = System.Drawing.Color.Transparent;
            this.lblBtn.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBtn.Location = new System.Drawing.Point(226, 441);
            this.lblBtn.Name = "lblBtn";
            this.lblBtn.Size = new System.Drawing.Size(710, 29);
            this.lblBtn.TabIndex = 5;
            this.lblBtn.Text = "Cliquez sur un des deux boutons ci-dessous pour commencer la partie";
            // 
            // lblRegle
            // 
            this.lblRegle.AutoSize = true;
            this.lblRegle.BackColor = System.Drawing.Color.Transparent;
            this.lblRegle.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegle.Location = new System.Drawing.Point(832, 625);
            this.lblRegle.Name = "lblRegle";
            this.lblRegle.Size = new System.Drawing.Size(157, 29);
            this.lblRegle.TabIndex = 6;
            this.lblRegle.Text = "Règles du jeu :";
            // 
            // FrmAccueil
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1100, 700);
            this.Controls.Add(this.lblRegle);
            this.Controls.Add(this.lblBtn);
            this.Controls.Add(this.lblBut);
            this.Controls.Add(this.iPcbRegle);
            this.Controls.Add(this.btnEnfant);
            this.Controls.Add(this.btnAdulte);
            this.Controls.Add(this.btnQuitter);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmAccueil";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.iPcbRegle)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnQuitter;
        private System.Windows.Forms.Button btnAdulte;
        private System.Windows.Forms.Button btnEnfant;
        private FontAwesome.Sharp.IconPictureBox iPcbRegle;
        private System.Windows.Forms.Label lblBut;
        private System.Windows.Forms.Label lblBtn;
        private System.Windows.Forms.Label lblRegle;
    }
}

