﻿
namespace ProjetTutore
{
    partial class frmRegles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRegles));
            this.lblRegle = new System.Windows.Forms.Label();
            this.lblRegle2 = new System.Windows.Forms.Label();
            this.btnRetour = new System.Windows.Forms.Button();
            this.pcbDec = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pcbDec)).BeginInit();
            this.SuspendLayout();
            // 
            // lblRegle
            // 
            this.lblRegle.AutoSize = true;
            this.lblRegle.BackColor = System.Drawing.Color.Transparent;
            this.lblRegle.Font = new System.Drawing.Font("Calibri", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegle.Location = new System.Drawing.Point(479, 9);
            this.lblRegle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRegle.Name = "lblRegle";
            this.lblRegle.Size = new System.Drawing.Size(120, 45);
            this.lblRegle.TabIndex = 3;
            this.lblRegle.Text = "Règles";
            // 
            // lblRegle2
            // 
            this.lblRegle2.BackColor = System.Drawing.Color.White;
            this.lblRegle2.Font = new System.Drawing.Font("Calibri", 13.8F);
            this.lblRegle2.Location = new System.Drawing.Point(13, 64);
            this.lblRegle2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRegle2.Name = "lblRegle2";
            this.lblRegle2.Size = new System.Drawing.Size(1074, 271);
            this.lblRegle2.TabIndex = 4;
            this.lblRegle2.Text = resources.GetString("lblRegle2.Text");
            // 
            // btnRetour
            // 
            this.btnRetour.BackColor = System.Drawing.Color.White;
            this.btnRetour.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRetour.Location = new System.Drawing.Point(929, 639);
            this.btnRetour.Name = "btnRetour";
            this.btnRetour.Size = new System.Drawing.Size(158, 49);
            this.btnRetour.TabIndex = 5;
            this.btnRetour.Text = "Retour";
            this.btnRetour.UseVisualStyleBackColor = false;
            this.btnRetour.Click += new System.EventHandler(this.btnQuitter_Click);
            // 
            // pcbDec
            // 
            this.pcbDec.Image = ((System.Drawing.Image)(resources.GetObject("pcbDec.Image")));
            this.pcbDec.Location = new System.Drawing.Point(254, 353);
            this.pcbDec.Name = "pcbDec";
            this.pcbDec.Size = new System.Drawing.Size(604, 283);
            this.pcbDec.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcbDec.TabIndex = 6;
            this.pcbDec.TabStop = false;
            // 
            // frmRegles
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1100, 700);
            this.Controls.Add(this.pcbDec);
            this.Controls.Add(this.btnRetour);
            this.Controls.Add(this.lblRegle2);
            this.Controls.Add(this.lblRegle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmRegles";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmRegles";
            ((System.ComponentModel.ISupportInitialize)(this.pcbDec)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblRegle;
        private System.Windows.Forms.Label lblRegle2;
        private System.Windows.Forms.Button btnRetour;
        private System.Windows.Forms.PictureBox pcbDec;
    }
}