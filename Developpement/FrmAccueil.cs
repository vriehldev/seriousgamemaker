﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetTutore
{
    /// <summary>
    /// Formulaire d'accueil de jeu 
    /// </summary>
    public partial class FrmAccueil : Form
    {
        /// <summary>
        /// Indique la cible du jeu sérieux créé
        /// </summary>
        /// <remarks>La cible vaut 0 pour la cible "Adulte" ou 1 pour la cible "Enfant"</remarks>
        int cible = 0;

        /// <summary>
        /// Constructeur du formulaire d'accueil 
        /// </summary>
        public FrmAccueil()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Lorsque le joueur clique sur le bouton "Quitter"
        /// </summary>
        /// <param name="sender">Object ayant demandé l'évènement</param>
        /// <param name="e">Arguments d'évènement</param>
        private void btnQuitter_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Lorsque le joueur clique sur le bouton "Adulte"
        /// </summary>
        /// <param name="sender">Object ayant demandé l'évènement</param>
        /// <param name="e">Arguments d'évènement</param>
        private void btnAdulte_Click(object sender, EventArgs e)
        {
            try
            {
                //Un nouveau formulaire de décision est créé
                FrmDecision newForm = new FrmDecision(cible);
                newForm.ShowDialog();
                //Le formulaire d'accueil est caché
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Lorsque le joueur clique sur le bouton "Enfant"
        /// </summary>
        /// <param name="sender">Object ayant demandé l'évènement</param>
        /// <param name="e">Arguments d'évènement</param>
        private void btnEnfant_Click(object sender, EventArgs e)
        {
            try
            {
                //Initialidation de la cible à 1 
                cible = 1;
                //Un nouveau formulaire de décision est créé 
                FrmDecision nf = new FrmDecision(cible);
                nf.ShowDialog();
                //Le formulaire d'accueil est caché 
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Si l'utilisateur clique sur l'icone "?" 
        /// </summary>
        /// <param name="sender">Object ayant demandé l'évènement</param>
        /// <param name="e">Arguments d'évènement</param>
        private void iPcbRegle_Click(object sender, EventArgs e)
        {
            try
            {
                //Le formulaire des régles est affiché 
                frmRegles form = new frmRegles();
                form.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}