﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.AccessControl;
using System.Data.OleDb;

namespace ProjetTutore
{
    /// <summary>
    /// Formulaire de décision du jeu 
    /// </summary>
    public partial class FrmDecision : Form
    {
        /// <summary>
        /// Indique la cible du jeu sérieux créé
        /// </summary>
        /// <remarks>La cible vaut 0 pour la cible "Adulte" ou 1 pour la cible "Enfant"</remarks>
        int codeCible;

        /// <summary>
        /// Constructeur du formulaire de décision 
        /// </summary>
        /// <param name="cible">Arguments d'évènement</param>
        public FrmDecision(int cible)
        {
            InitializeComponent();
            codeCible = cible;
        }

        /// <summary>
        /// Initialisation du nombre de pièces 
        /// </summary>
        int argent = 4;

        /// <summary>
        /// Initialisation du numéro de jour 
        /// </summary>
        int nbJour = 1;

        /// <summary>
        /// Initialisation des valeurs de décisions 
        /// </summary>
        int valeurDecision = 0;

        /// <summary>
        /// Initialisation d'une liste contenant chaque valeur de décision 
        /// </summary>
        List<int> valeurs = new List<int>();

        /// <summary>
        /// Initialisation d'une copie de la liste
        /// </summary>
        List<int> valeursDebutJour = new List<int>() { 0, 0, 0, 0, 0, 0, 0, 0 };

        /// <summary>
        /// Initialisation d'une pictureBox
        /// </summary>
        PictureBox pcb;

        /// <summary>
        /// Initialisation du bouton "moins" 
        /// </summary>
        Button btnMoins;

        /// <summary>
        /// Initialisation du bouton "plus"
        /// </summary>
        Button btnPlus;

        /// <summary>
        /// Initialisation de la chaîne de connection à la base de donnée 
        /// </summary>
        string chcon;

        /// <summary>
        /// Initialisation de la connection à la base de donnée 
        /// </summary>
        OleDbConnection connec;

        /// <summary>
        /// Création d'une copie de la table "Décisions" de la base de données 
        /// </summary>
        DataSet ds_decisions = new DataSet();

        /// <summary>
        /// Initialisation d'une ligne contenant les informations d'une décision 
        /// </summary>
        DataRow[] infoDecision;

        /// <summary>
        /// Initialisation d'une ligne contenant le message d'aide d'une décision
        /// </summary>
        DataRow[] infoAide;

        /// <summary>
        /// Initialisation du label permettant d'afficher le message d'aide d'une décision
        /// </summary>
        Label lblAide;

        /// <summary>
        /// Lorsque le joueur clique sur le bouton jourSuivant, le formulaire de récapitulatif s'ouvre et le jour augmente de 1
        /// </summary>
        /// <param name="sender">Object ayant demandé l'évènement</param>
        /// <param name="e"></param>
        private void btnPasser_Click(object sender, EventArgs e)
        {
            try
            {
                //On vide la copie de la liste valeurs 
                valeursDebutJour.Clear();

                //On remplit la copie de la liste de 0
                valeursDebutJour.AddRange(valeurs);

                //S'il s'agit du dernier jour de jeu
                if (nbJour == 10)
                {
                    //On affiche le formulaire de fin
                    frmFin newForm2 = new frmFin(valeurs, ds_decisions, codeCible);
                    newForm2.ShowDialog();
                }
                else
                {
                    //On incrémente le numéro de jour de 1
                    nbJour += 1;
                    //On incrémente le nombre de pièce de 4
                    argent += 4;
                    //On appel la précédure qui met à jour le panel
                    miseAJour();

                    //On affiche la nouvelle valeur du numéro de jour 
                    LblJour.Text = "Jour : " + nbJour + " / 10";
                    //On affiche la nouvelle valeur du nombre de pièce 
                    lblArgent.Text = argent.ToString();

                    //On affiche le formulaire récapitulatif 
                    frmRecap newForm = new frmRecap(valeurs, infoDecision, ds_decisions, codeCible, nbJour);
                    newForm.Show();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Lors du chargement du formulaire 
        /// </summary>
        /// <param name="sender">Object ayant demandé l'évènement</param>
        /// <param name="e">Arguments d'évènement</param>
        private void FrmDecision_Load(object sender, EventArgs e)
        {
            //Affichage du nombre de jour restant
            LblJour.Text = "Jour : " + nbJour + " / 10";
            lblArgent.Text = argent.ToString();

            //Connection à la base de données 
            chcon = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=.\ProjetT3.mdb";
            connec = new OleDbConnection();
            connec.ConnectionString = chcon;

            //Copie de la base de donnée dans le DataTable pour pouvoir y avoir accès en mode déconnecté
            try
            {
                connec.Open();

                DataTable schemaTable = connec.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

                for (int i = 0; i < schemaTable.Rows.Count; i++)
                {
                    string nomTable = schemaTable.Rows[i][2].ToString();
                    string requete = "select * from " + nomTable;

                    //objet command
                    OleDbCommand cmd = new OleDbCommand(requete, connec);
                    OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                    da.Fill(ds_decisions, nomTable);
                }

                //On ajoute la valeur 0 pour chaque décision dans la liste valeurs
                for (int i = 0; i < 8; i++)
                {
                    valeurs.Add(valeurDecision);
                }

                //On met à jour le panel
                miseAJour();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connec.Close();
            }
        }

        /// <summary>
        /// Mise à jour des décisions au début de chaque nouvelle journée
        /// </summary>
        private void miseAJour()
        {
            try
            {
                //Mise en place de la police d'écriture 
                Font font = new Font("Calibri", 20);

                //Nettoyage du panel 
                pnlDecision.Controls.Clear();

                //Initialition des variables utilisées pour placer les différents composants du formulaire
                int xLabel = 20;
                int x = 975;
                int y = 40;
                int xBtnMoins;
                int xPcb;

                //Recherche des informations sur les décisions se déroulant pendant le jour actuel
                infoDecision = ds_decisions.Tables["Decisions"].Select(nbJour + " >= jourDebut and " + nbJour + " <= jourFin");

                //Création des différents éléments pour chaque décisions 
                for (int i = 0; i < 4; i++)
                {
                    //Création d'un label
                    Label lblDecision = new Label();
                    pnlDecision.Controls.Add(lblDecision);
                    lblDecision.Location = new Point(xLabel, y);
                    lblDecision.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                    lblDecision.Text = (string)infoDecision[i]["nomDec"];
                    lblDecision.Font = font;
                    lblDecision.AutoSize = true;
                    lblDecision.MouseHover += new System.EventHandler(this.lblDecision_MouseHover);
                    lblDecision.MouseLeave += new System.EventHandler(this.lblDecision_MouseLeave);
                    lblDecision.Tag = (int)infoDecision[i]["codeDec"];

                    //Création d'un bouton "+"
                    btnPlus = new Button();
                    pnlDecision.Controls.Add(btnPlus);
                    btnPlus.Location = new Point(x, y);
                    btnPlus.Size = new Size(37, 37);
                    btnPlus.BackgroundImage = System.Drawing.Image.FromFile(System.Windows.Forms.Application.StartupPath + @"\Images\plus.png");
                    btnPlus.BackgroundImageLayout = ImageLayout.Stretch;
                    btnPlus.FlatStyle = FlatStyle.Flat;
                    btnPlus.FlatAppearance.BorderColor = Color.White;
                    btnPlus.FlatAppearance.MouseOverBackColor = Color.White;
                    btnPlus.FlatAppearance.MouseDownBackColor = Color.White;
                    btnPlus.Click += new EventHandler(this.btnPlus_Click);
                    btnPlus.Tag = (int)infoDecision[i]["codeDec"];

                    //Création d'une pictureBox
                    pcb = new PictureBox();
                    pnlDecision.Controls.Add(pcb);
                    pcb.Size = new Size(350, 40);
                    pcb.SizeMode = PictureBoxSizeMode.StretchImage;
                    pcb.Tag = (int)infoDecision[i]["codeDec"];

                    //Création d'un bouton "-"
                    btnMoins = new Button();
                    pnlDecision.Controls.Add(btnMoins);
                    btnMoins.Size = new Size(37, 37);
                    btnMoins.BackgroundImage = System.Drawing.Image.FromFile(System.Windows.Forms.Application.StartupPath + @"\Images\moins.png");
                    btnMoins.BackgroundImageLayout = ImageLayout.Stretch;
                    btnMoins.FlatStyle = FlatStyle.Flat;
                    btnMoins.FlatAppearance.BorderColor = Color.White;
                    btnMoins.FlatAppearance.MouseOverBackColor = Color.White;
                    btnMoins.FlatAppearance.MouseDownBackColor = Color.White;
                    btnMoins.Click += new System.EventHandler(this.btnMoins_Click);
                    btnMoins.Tag = (int)infoDecision[i]["codeDec"];

                    //Placement des différents éléments par rapport aux autres 
                    xPcb = btnPlus.Left - (pcb.Right - pcb.Left) - 30;
                    pcb.Location = new Point(xPcb, y);

                    xBtnMoins = pcb.Left - (btnMoins.Right - btnMoins.Left) - 30;
                    btnMoins.Location = new Point(xBtnMoins, y);

                    //Mise à jour de la progressBar affichée dans la pictureBox
                    EtatPgb((int)pcb.Tag);

                    //Décalage vertical afin d'afficher la prochaine décision
                    y += 125;

                    //Si le jour actuel est le dernier jour du jeu, changement du texte du bouton
                    if (nbJour == 10)
                    {
                        btnPasser.Text = "Fin";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Lorsque le joueur clique sur le bouton "-"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMoins_Click(object sender, EventArgs e)
        {
            try
            {
                //On récupère le code de la décision sur laquelle le joueur veut agir
                int pos = (int)((Button)sender).Tag;

                //Si le joueur n'a plus d'argent, on affiche un message d'erreur
                if (argent == 0 && valeurs[pos - 1] - 1 < valeursDebutJour[pos - 1])
                {
                    MessageBox.Show("Vous n'avez plus assez d'argent", "Attention", 0, MessageBoxIcon.Error);
                }
                else
                {
                    //Si la valeur est différente de 0
                    if (valeurs[pos - 1] != 0)
                    {
                        //Alors la valeur de la décision est décrémentée 
                        valeurs[pos - 1] -= 1;

                        //Si le joueur prend une décision
                        if (valeurs[pos - 1] < valeursDebutJour[pos - 1])
                        {
                            //Il perd de l'argent
                            argent -= 1;
                        }
                        //Mais si il revient sur un choix fait au cours de la même journée
                        else
                        {
                            //Il regagne l'argent perdu
                            argent += 1;
                        }

                        //On met à jour le nombre de pièce disponible
                        lblArgent.Text = argent.ToString();

                        //Si le joueur diminue de 2 la décision Quantité d'information
                        if (pos == 7 && valeurs[pos - 1] % 2 == 0 && valeurs[6 - 1] != 0)
                            //Alors la décision Difficulté dimunue de 1
                            valeurs[6 - 1] -= 1;

                        //Si le joueur diminue de 2 la décision Feedback
                        if (pos == 4 && valeurs[pos - 1] % 2 == 0)
                        {
                            //La décision Difficulté augmente de 1
                            if (valeurs[6 - 1] != 10)
                                valeurs[6 - 1] += 1;

                            //La décision Expérience active de maîtrise diminue de 1
                            if (valeurs[8 - 1] != 0)
                                valeurs[8 - 1] -= 1;
                        }
                        //Si la décision Courbe de progression diminue de 2
                        if (pos == 5 && valeurs[pos - 1] % 2 == 0 && valeurs[6 - 1] != 0)
                        {
                            //La décision difficulté diminue de 1
                            valeurs[6 - 1] -= 1;
                        }
                    }

                }

                //On met à jour l'état de la progressBar
                foreach (Control c in pnlDecision.Controls)
                {
                    if (c is PictureBox)
                    {
                        EtatPgb((int)c.Tag);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Lorsque le joueur clique sur le bouton "+"
        /// </summary>
        /// <param name="sender">Object ayant demandé l'évènement</param>
        /// <param name="e">Arguments d'évènement</param>
        private void btnPlus_Click(object sender, EventArgs e)
        {
            try
            {
                //On récupère le code de la décision sur laquelle le joueur veut agir
                int pos = (int)((Button)sender).Tag;

                //Si le joueur n'a plus d'argent, on affiche un message d'erreur
                if (argent == 0 && valeurs[pos - 1] + 1 > valeursDebutJour[pos - 1])
                {
                    MessageBox.Show("Vous n'avez plus assez d'argent", "Attention", 0, MessageBoxIcon.Error);
                }
                else
                {
                    //Si la valeur de la décision est différente de 10
                    if (valeurs[pos - 1] != 10)
                    {
                        //Alors la valeur de la décision est incrémentée de 1 
                        valeurs[pos - 1] += 1;

                        //Si le joueur prend une décision
                        if (valeurs[pos - 1] > valeursDebutJour[pos - 1])
                        {
                            //Le joueur perd de l'argent 
                            argent -= 1;
                        }
                        //Mais si il revient sur un choix fait au cours de la même journée
                        else
                        {
                            //Il regagne l'argent perdu
                            argent += 1;
                        }

                        //On met à jour le nombre de pièces disponible
                        lblArgent.Text = argent.ToString();

                        //Si le joueur augmente de 2 la décision Quantité d'information
                        if (pos == 7 && valeurs[pos - 1] % 2 == 0 && valeurs[6 - 1] != 10)
                            valeurs[6 - 1] += 1;

                        //Si le joueur augmente de 2 la décision Feedback
                        if (pos == 4 && valeurs[pos - 1] % 2 == 0)
                        {
                            //La décision Difficulté diminue de 1
                            if (valeurs[6 - 1] != 0)
                                valeurs[6 - 1] -= 1;

                            //La décision Expérience active de maîtrise augmente de 1
                            if (valeurs[8 - 1] != 10)
                                valeurs[8 - 1] += 1;
                        }

                        //Si la décision Courbe de progression augmente de 2
                        if (pos == 5 && valeurs[pos - 1] % 2 == 0 && valeurs[6 - 1] != 10)
                            //La décision difficulté augmente de 1
                            valeurs[6 - 1] += 1;
                    }

                }

                //On met à jour l'état de la progressBar
                foreach (Control c in pnlDecision.Controls)
                {
                    if (c is PictureBox)
                    {
                        EtatPgb((int)c.Tag);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Mise à jour de la progressBar 
        /// </summary>
        /// <param name="tag">Code de la décision</param>
        private void EtatPgb(int tag)
        {
            try
            {
                //Pour chaque control contenu dans le panel
                foreach (Control c in pnlDecision.Controls)
                {
                    //On vérifit qu'il s'agit d'une pictureBox
                    if (c is PictureBox)
                    {
                        //Si son tag correspond au tag du bouton actionné
                        if ((int)c.Tag == tag)
                        {
                            //Alors on met à jour la pictureBox
                            ((PictureBox)c).Image = System.Drawing.Image.FromFile(System.Windows.Forms.Application.StartupPath + @"\Images\pgb" + valeurs[tag - 1] + ".png");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Lorsque le joueur met sa souris au-dessus du label d'une décision
        /// </summary>
        /// <param name="sender">Object ayant demandé l'évènement</param>
        /// <param name="e">Arguments d'évènement</param>
        private void lblDecision_MouseHover(object sender, EventArgs e)
        {
            try
            {
                //On récupère les informations de cette décison
                infoAide = ds_decisions.Tables["Decisions"].Select("codeDec = " + ((Label)sender).Tag);

                //On récupère le message d'aide associé à cette décision 
                string aide = infoAide[0]["aide"].ToString();

                //On affiche le message d'aide en bas du panel de décisions
                lblAide = new Label();
                pnlDecision.Controls.Add(lblAide);
                lblAide.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                lblAide.Location = new System.Drawing.Point(3, 482);
                lblAide.Size = new System.Drawing.Size(1063, 87);
                lblAide.TabIndex = 0;
                lblAide.Text = aide;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Lorsque le joueur enlève sa souris du label d'une décision
        /// </summary>
        /// <param name="sender">Object ayant demandé l'évènement</param>
        /// <param name="e">Arguments d'évènement</param>
        private void lblDecision_MouseLeave(object sender, EventArgs e)
        {
            try
            {
                //Suppression du message d'aide du panel de décisions
                lblAide.Text = "";
                pnlDecision.Controls.Remove(lblAide);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Lorsque le joueur clique sur le bouton "?"
        /// </summary>
        /// <param name="sender">Object ayant demandé l'évènement</param>
        /// <param name="e">Arguments d'évènement</param>
        private void iPcbRegle_Click_1(object sender, EventArgs e)
        {
            try
            {
                //Le formulaire d'aide est affiché
                frmRegles frmR = new frmRegles();
                frmR.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Lorsque le joueur clique sur le bouton "Menu principal"
        /// </summary>
        /// <param name="sender">Object ayant demandé l'évènement</param>
        /// <param name="e">Arguments d'évènement</param>
        private void btnMenu_Click(object sender, EventArgs e)
        {
            try
            {
                //Le formulaire d'accueil est affiché
                FrmAccueil frmAcc = new FrmAccueil();
                frmAcc.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
