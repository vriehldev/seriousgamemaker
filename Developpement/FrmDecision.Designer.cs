﻿
namespace ProjetTutore
{
    partial class FrmDecision
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDecision));
            this.btnPasser = new System.Windows.Forms.Button();
            this.pnlDecision = new System.Windows.Forms.Panel();
            this.pcbPiece = new System.Windows.Forms.PictureBox();
            this.LblJour = new System.Windows.Forms.Label();
            this.lblArgent = new System.Windows.Forms.Label();
            this.iPcbRegle = new FontAwesome.Sharp.IconPictureBox();
            this.btnMenu = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pcbPiece)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iPcbRegle)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnPasser
            // 
            this.btnPasser.BackColor = System.Drawing.Color.White;
            this.btnPasser.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPasser.Location = new System.Drawing.Point(890, 639);
            this.btnPasser.Name = "btnPasser";
            this.btnPasser.Size = new System.Drawing.Size(198, 49);
            this.btnPasser.TabIndex = 1;
            this.btnPasser.Text = "Jour suivant";
            this.btnPasser.UseVisualStyleBackColor = false;
            this.btnPasser.Click += new System.EventHandler(this.btnPasser_Click);
            // 
            // pnlDecision
            // 
            this.pnlDecision.BackColor = System.Drawing.Color.White;
            this.pnlDecision.Location = new System.Drawing.Point(13, 70);
            this.pnlDecision.Name = "pnlDecision";
            this.pnlDecision.Size = new System.Drawing.Size(1074, 557);
            this.pnlDecision.TabIndex = 2;
            // 
            // pcbPiece
            // 
            this.pcbPiece.Image = ((System.Drawing.Image)(resources.GetObject("pcbPiece.Image")));
            this.pcbPiece.Location = new System.Drawing.Point(173, -39);
            this.pcbPiece.Name = "pcbPiece";
            this.pcbPiece.Size = new System.Drawing.Size(172, 141);
            this.pcbPiece.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcbPiece.TabIndex = 3;
            this.pcbPiece.TabStop = false;
            // 
            // LblJour
            // 
            this.LblJour.AutoSize = true;
            this.LblJour.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblJour.Location = new System.Drawing.Point(19, 15);
            this.LblJour.Name = "LblJour";
            this.LblJour.Size = new System.Drawing.Size(0, 35);
            this.LblJour.TabIndex = 4;
            // 
            // lblArgent
            // 
            this.lblArgent.AutoSize = true;
            this.lblArgent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(202)))), ((int)(((byte)(208)))));
            this.lblArgent.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblArgent.Location = new System.Drawing.Point(266, 15);
            this.lblArgent.Name = "lblArgent";
            this.lblArgent.Size = new System.Drawing.Size(0, 35);
            this.lblArgent.TabIndex = 5;
            // 
            // iPcbRegle
            // 
            this.iPcbRegle.BackColor = System.Drawing.Color.Transparent;
            this.iPcbRegle.ForeColor = System.Drawing.SystemColors.ControlText;
            this.iPcbRegle.IconChar = FontAwesome.Sharp.IconChar.QuestionCircle;
            this.iPcbRegle.IconColor = System.Drawing.SystemColors.ControlText;
            this.iPcbRegle.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iPcbRegle.IconSize = 60;
            this.iPcbRegle.Location = new System.Drawing.Point(12, 4);
            this.iPcbRegle.Name = "iPcbRegle";
            this.iPcbRegle.Size = new System.Drawing.Size(60, 60);
            this.iPcbRegle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.iPcbRegle.TabIndex = 6;
            this.iPcbRegle.TabStop = false;
            this.iPcbRegle.Click += new System.EventHandler(this.iPcbRegle_Click_1);
            // 
            // btnMenu
            // 
            this.btnMenu.BackColor = System.Drawing.Color.White;
            this.btnMenu.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMenu.Location = new System.Drawing.Point(13, 639);
            this.btnMenu.Name = "btnMenu";
            this.btnMenu.Size = new System.Drawing.Size(198, 49);
            this.btnMenu.TabIndex = 7;
            this.btnMenu.Text = "Menu principal";
            this.btnMenu.UseVisualStyleBackColor = false;
            this.btnMenu.Click += new System.EventHandler(this.btnMenu_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.lblArgent);
            this.panel1.Controls.Add(this.pcbPiece);
            this.panel1.Controls.Add(this.LblJour);
            this.panel1.Location = new System.Drawing.Point(701, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(345, 60);
            this.panel1.TabIndex = 8;
            // 
            // FrmDecision
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1100, 700);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnMenu);
            this.Controls.Add(this.iPcbRegle);
            this.Controls.Add(this.pnlDecision);
            this.Controls.Add(this.btnPasser);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmDecision";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.FrmDecision_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pcbPiece)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iPcbRegle)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnPasser;
        private System.Windows.Forms.Panel pnlDecision;
        private System.Windows.Forms.PictureBox pcbPiece;
        private System.Windows.Forms.Label LblJour;
        private System.Windows.Forms.Label lblArgent;
        private FontAwesome.Sharp.IconPictureBox iPcbRegle;
        private System.Windows.Forms.Button btnMenu;
        private System.Windows.Forms.Panel panel1;
    }
}