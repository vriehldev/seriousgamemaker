﻿
namespace ProjetTutore
{
    partial class frmFin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFin));
            this.pnlRecap = new System.Windows.Forms.Panel();
            this.lblLF = new System.Windows.Forms.Label();
            this.lblSGN = new System.Windows.Forms.Label();
            this.lblJvs = new System.Windows.Forms.Label();
            this.pcbLF = new System.Windows.Forms.PictureBox();
            this.pcbSGN = new System.Windows.Forms.PictureBox();
            this.pcbJvs = new System.Windows.Forms.PictureBox();
            this.pcbExpActivMaitrise = new System.Windows.Forms.PictureBox();
            this.pcbQteInfos = new System.Windows.Forms.PictureBox();
            this.pcbDifficulte = new System.Windows.Forms.PictureBox();
            this.pcbCourbeProg = new System.Windows.Forms.PictureBox();
            this.pcbFeedback = new System.Windows.Forms.PictureBox();
            this.pcbDureeVie = new System.Windows.Forms.PictureBox();
            this.pcbDegLib = new System.Windows.Forms.PictureBox();
            this.pcbDirAr = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblQteInfos = new System.Windows.Forms.Label();
            this.lblDifficulte = new System.Windows.Forms.Label();
            this.lblCourbeProg = new System.Windows.Forms.Label();
            this.lblFeedback = new System.Windows.Forms.Label();
            this.lblDureeVie = new System.Windows.Forms.Label();
            this.lblDegLib = new System.Windows.Forms.Label();
            this.lblDirArt = new System.Windows.Forms.Label();
            this.btnAccueil = new System.Windows.Forms.Button();
            this.lblFin = new System.Windows.Forms.Label();
            this.pnlRecap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbLF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbSGN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbJvs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbExpActivMaitrise)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbQteInfos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbDifficulte)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbCourbeProg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbFeedback)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbDureeVie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbDegLib)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbDirAr)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlRecap
            // 
            this.pnlRecap.BackColor = System.Drawing.Color.White;
            this.pnlRecap.Controls.Add(this.lblLF);
            this.pnlRecap.Controls.Add(this.lblSGN);
            this.pnlRecap.Controls.Add(this.lblJvs);
            this.pnlRecap.Controls.Add(this.pcbLF);
            this.pnlRecap.Controls.Add(this.pcbSGN);
            this.pnlRecap.Controls.Add(this.pcbJvs);
            this.pnlRecap.Controls.Add(this.pcbExpActivMaitrise);
            this.pnlRecap.Controls.Add(this.pcbQteInfos);
            this.pnlRecap.Controls.Add(this.pcbDifficulte);
            this.pnlRecap.Controls.Add(this.pcbCourbeProg);
            this.pnlRecap.Controls.Add(this.pcbFeedback);
            this.pnlRecap.Controls.Add(this.pcbDureeVie);
            this.pnlRecap.Controls.Add(this.pcbDegLib);
            this.pnlRecap.Controls.Add(this.pcbDirAr);
            this.pnlRecap.Controls.Add(this.label1);
            this.pnlRecap.Controls.Add(this.lblQteInfos);
            this.pnlRecap.Controls.Add(this.lblDifficulte);
            this.pnlRecap.Controls.Add(this.lblCourbeProg);
            this.pnlRecap.Controls.Add(this.lblFeedback);
            this.pnlRecap.Controls.Add(this.lblDureeVie);
            this.pnlRecap.Controls.Add(this.lblDegLib);
            this.pnlRecap.Controls.Add(this.lblDirArt);
            this.pnlRecap.Location = new System.Drawing.Point(12, 73);
            this.pnlRecap.Name = "pnlRecap";
            this.pnlRecap.Size = new System.Drawing.Size(1076, 554);
            this.pnlRecap.TabIndex = 3;
            // 
            // lblLF
            // 
            this.lblLF.AutoSize = true;
            this.lblLF.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLF.Location = new System.Drawing.Point(685, 416);
            this.lblLF.Name = "lblLF";
            this.lblLF.Size = new System.Drawing.Size(0, 24);
            this.lblLF.TabIndex = 21;
            // 
            // lblSGN
            // 
            this.lblSGN.AutoSize = true;
            this.lblSGN.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSGN.Location = new System.Drawing.Point(204, 513);
            this.lblSGN.Name = "lblSGN";
            this.lblSGN.Size = new System.Drawing.Size(0, 24);
            this.lblSGN.TabIndex = 20;
            // 
            // lblJvs
            // 
            this.lblJvs.AutoSize = true;
            this.lblJvs.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJvs.Location = new System.Drawing.Point(29, 416);
            this.lblJvs.Name = "lblJvs";
            this.lblJvs.Size = new System.Drawing.Size(0, 24);
            this.lblJvs.TabIndex = 19;
            // 
            // pcbLF
            // 
            this.pcbLF.Image = ((System.Drawing.Image)(resources.GetObject("pcbLF.Image")));
            this.pcbLF.Location = new System.Drawing.Point(785, 293);
            this.pcbLF.Name = "pcbLF";
            this.pcbLF.Size = new System.Drawing.Size(183, 112);
            this.pcbLF.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcbLF.TabIndex = 18;
            this.pcbLF.TabStop = false;
            // 
            // pcbSGN
            // 
            this.pcbSGN.Image = ((System.Drawing.Image)(resources.GetObject("pcbSGN.Image")));
            this.pcbSGN.Location = new System.Drawing.Point(437, 404);
            this.pcbSGN.Name = "pcbSGN";
            this.pcbSGN.Size = new System.Drawing.Size(181, 147);
            this.pcbSGN.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcbSGN.TabIndex = 17;
            this.pcbSGN.TabStop = false;
            // 
            // pcbJvs
            // 
            this.pcbJvs.Image = ((System.Drawing.Image)(resources.GetObject("pcbJvs.Image")));
            this.pcbJvs.Location = new System.Drawing.Point(68, 303);
            this.pcbJvs.Name = "pcbJvs";
            this.pcbJvs.Size = new System.Drawing.Size(136, 102);
            this.pcbJvs.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcbJvs.TabIndex = 16;
            this.pcbJvs.TabStop = false;
            // 
            // pcbExpActivMaitrise
            // 
            this.pcbExpActivMaitrise.Location = new System.Drawing.Point(821, 228);
            this.pcbExpActivMaitrise.Name = "pcbExpActivMaitrise";
            this.pcbExpActivMaitrise.Size = new System.Drawing.Size(250, 37);
            this.pcbExpActivMaitrise.TabIndex = 15;
            this.pcbExpActivMaitrise.TabStop = false;
            // 
            // pcbQteInfos
            // 
            this.pcbQteInfos.Location = new System.Drawing.Point(821, 161);
            this.pcbQteInfos.Name = "pcbQteInfos";
            this.pcbQteInfos.Size = new System.Drawing.Size(250, 37);
            this.pcbQteInfos.TabIndex = 14;
            this.pcbQteInfos.TabStop = false;
            // 
            // pcbDifficulte
            // 
            this.pcbDifficulte.Location = new System.Drawing.Point(821, 92);
            this.pcbDifficulte.Name = "pcbDifficulte";
            this.pcbDifficulte.Size = new System.Drawing.Size(250, 37);
            this.pcbDifficulte.TabIndex = 13;
            this.pcbDifficulte.TabStop = false;
            // 
            // pcbCourbeProg
            // 
            this.pcbCourbeProg.Location = new System.Drawing.Point(821, 22);
            this.pcbCourbeProg.Name = "pcbCourbeProg";
            this.pcbCourbeProg.Size = new System.Drawing.Size(250, 37);
            this.pcbCourbeProg.TabIndex = 12;
            this.pcbCourbeProg.TabStop = false;
            // 
            // pcbFeedback
            // 
            this.pcbFeedback.Location = new System.Drawing.Point(225, 228);
            this.pcbFeedback.Name = "pcbFeedback";
            this.pcbFeedback.Size = new System.Drawing.Size(250, 37);
            this.pcbFeedback.TabIndex = 11;
            this.pcbFeedback.TabStop = false;
            // 
            // pcbDureeVie
            // 
            this.pcbDureeVie.Location = new System.Drawing.Point(225, 161);
            this.pcbDureeVie.Name = "pcbDureeVie";
            this.pcbDureeVie.Size = new System.Drawing.Size(250, 37);
            this.pcbDureeVie.TabIndex = 10;
            this.pcbDureeVie.TabStop = false;
            // 
            // pcbDegLib
            // 
            this.pcbDegLib.Location = new System.Drawing.Point(225, 92);
            this.pcbDegLib.Name = "pcbDegLib";
            this.pcbDegLib.Size = new System.Drawing.Size(250, 37);
            this.pcbDegLib.TabIndex = 9;
            this.pcbDegLib.TabStop = false;
            // 
            // pcbDirAr
            // 
            this.pcbDirAr.Location = new System.Drawing.Point(225, 22);
            this.pcbDirAr.Name = "pcbDirAr";
            this.pcbDirAr.Size = new System.Drawing.Size(250, 37);
            this.pcbDirAr.TabIndex = 8;
            this.pcbDirAr.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(516, 236);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(299, 29);
            this.label1.TabIndex = 7;
            this.label1.Text = "Expérience active de maîtrise";
            // 
            // lblQteInfos
            // 
            this.lblQteInfos.AutoSize = true;
            this.lblQteInfos.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQteInfos.Location = new System.Drawing.Point(516, 169);
            this.lblQteInfos.Name = "lblQteInfos";
            this.lblQteInfos.Size = new System.Drawing.Size(248, 29);
            this.lblQteInfos.TabIndex = 6;
            this.lblQteInfos.Text = "Quantité d\'informations";
            // 
            // lblDifficulte
            // 
            this.lblDifficulte.AutoSize = true;
            this.lblDifficulte.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDifficulte.Location = new System.Drawing.Point(516, 100);
            this.lblDifficulte.Name = "lblDifficulte";
            this.lblDifficulte.Size = new System.Drawing.Size(102, 29);
            this.lblDifficulte.TabIndex = 5;
            this.lblDifficulte.Text = "Difficulté";
            // 
            // lblCourbeProg
            // 
            this.lblCourbeProg.AutoSize = true;
            this.lblCourbeProg.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCourbeProg.Location = new System.Drawing.Point(516, 30);
            this.lblCourbeProg.Name = "lblCourbeProg";
            this.lblCourbeProg.Size = new System.Drawing.Size(235, 29);
            this.lblCourbeProg.TabIndex = 4;
            this.lblCourbeProg.Text = "Courbe de progression";
            // 
            // lblFeedback
            // 
            this.lblFeedback.AutoSize = true;
            this.lblFeedback.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFeedback.Location = new System.Drawing.Point(3, 236);
            this.lblFeedback.Name = "lblFeedback";
            this.lblFeedback.Size = new System.Drawing.Size(107, 29);
            this.lblFeedback.TabIndex = 3;
            this.lblFeedback.Text = "Feedback";
            // 
            // lblDureeVie
            // 
            this.lblDureeVie.AutoSize = true;
            this.lblDureeVie.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDureeVie.Location = new System.Drawing.Point(3, 169);
            this.lblDureeVie.Name = "lblDureeVie";
            this.lblDureeVie.Size = new System.Drawing.Size(142, 29);
            this.lblDureeVie.TabIndex = 2;
            this.lblDureeVie.Text = "Durée de vie ";
            // 
            // lblDegLib
            // 
            this.lblDegLib.AutoSize = true;
            this.lblDegLib.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDegLib.Location = new System.Drawing.Point(3, 100);
            this.lblDegLib.Name = "lblDegLib";
            this.lblDegLib.Size = new System.Drawing.Size(176, 29);
            this.lblDegLib.TabIndex = 1;
            this.lblDegLib.Text = "Degré de liberté ";
            // 
            // lblDirArt
            // 
            this.lblDirArt.AutoSize = true;
            this.lblDirArt.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDirArt.Location = new System.Drawing.Point(3, 30);
            this.lblDirArt.Name = "lblDirArt";
            this.lblDirArt.Size = new System.Drawing.Size(201, 29);
            this.lblDirArt.TabIndex = 0;
            this.lblDirArt.Text = "Direction artistique";
            // 
            // btnAccueil
            // 
            this.btnAccueil.BackColor = System.Drawing.Color.White;
            this.btnAccueil.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAccueil.Location = new System.Drawing.Point(885, 645);
            this.btnAccueil.Name = "btnAccueil";
            this.btnAccueil.Size = new System.Drawing.Size(202, 46);
            this.btnAccueil.TabIndex = 4;
            this.btnAccueil.Text = "Menu principal";
            this.btnAccueil.UseVisualStyleBackColor = false;
            this.btnAccueil.Click += new System.EventHandler(this.btnAccueil_Click);
            // 
            // lblFin
            // 
            this.lblFin.AutoSize = true;
            this.lblFin.BackColor = System.Drawing.Color.Transparent;
            this.lblFin.Font = new System.Drawing.Font("Calibri", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFin.Location = new System.Drawing.Point(414, 25);
            this.lblFin.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFin.Name = "lblFin";
            this.lblFin.Size = new System.Drawing.Size(191, 45);
            this.lblFin.TabIndex = 5;
            this.lblFin.Text = "Note finale";
            // 
            // frmFin
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1100, 700);
            this.Controls.Add(this.lblFin);
            this.Controls.Add(this.btnAccueil);
            this.Controls.Add(this.pnlRecap);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmFin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmFin";
            this.Load += new System.EventHandler(this.frmFin_Load);
            this.pnlRecap.ResumeLayout(false);
            this.pnlRecap.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbLF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbSGN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbJvs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbExpActivMaitrise)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbQteInfos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbDifficulte)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbCourbeProg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbFeedback)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbDureeVie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbDegLib)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbDirAr)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlRecap;
        private System.Windows.Forms.PictureBox pcbExpActivMaitrise;
        private System.Windows.Forms.PictureBox pcbQteInfos;
        private System.Windows.Forms.PictureBox pcbDifficulte;
        private System.Windows.Forms.PictureBox pcbCourbeProg;
        private System.Windows.Forms.PictureBox pcbFeedback;
        private System.Windows.Forms.PictureBox pcbDureeVie;
        private System.Windows.Forms.PictureBox pcbDegLib;
        private System.Windows.Forms.PictureBox pcbDirAr;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblQteInfos;
        private System.Windows.Forms.Label lblDifficulte;
        private System.Windows.Forms.Label lblCourbeProg;
        private System.Windows.Forms.Label lblFeedback;
        private System.Windows.Forms.Label lblDureeVie;
        private System.Windows.Forms.Label lblDegLib;
        private System.Windows.Forms.Label lblDirArt;
        private System.Windows.Forms.Button btnAccueil;
        private System.Windows.Forms.Label lblFin;
        private System.Windows.Forms.Label lblJvs;
        private System.Windows.Forms.PictureBox pcbLF;
        private System.Windows.Forms.PictureBox pcbSGN;
        private System.Windows.Forms.PictureBox pcbJvs;
        private System.Windows.Forms.Label lblSGN;
        private System.Windows.Forms.Label lblLF;
    }
}